﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using TesTechnical.Models;

namespace TesTechnical.Controllers
{
    public class ListEmployeesController : Controller
    {
        private readonly TesTechnicalContext _context;

        public ListEmployeesController(TesTechnicalContext context)
        {
            _context = context;
        }

        // GET: ListEmployees
        public async Task<IActionResult> Index()
        {
            return View(await _context.ListEmployee.ToListAsync());
        }

        // GET: ListEmployees/Details/5
        public async Task<IActionResult> Details(decimal? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var listEmployee = await _context.ListEmployee
                .FirstOrDefaultAsync(m => m.Id == id);
            if (listEmployee == null)
            {
                return NotFound();
            }

            return View(listEmployee);
        }

        //[HttpGet]

        //public async Task<IActionResult> Index(string Empsearch, string sortingemp)
        //{
        //    ViewData["ListEmployee"] = Empsearch;
        //    ViewData["EmployeeName"] = string.IsNullOrEmpty(sortingemp) ? "NIK" : "";

        //    var empquery = from x in ListEmployee select x;
        //}

        //public ActionResult Index(string sortOrder)
        //{
        //    ViewBag.NameSortParm = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
        //    ViewBag.NameSortParm = String.IsNullOrEmpty(sortOrder) ? "gender" : "gender_desc";
        //    ViewBag.NameSortParm = String.IsNullOrEmpty(sortOrder) ? "departemen" : "departemen_desc";
        //    ViewBag.NameSortParm = String.IsNullOrEmpty(sortOrder) ? "jabatan" : "jabatan_desc";
        //    ViewBag.DateSortParm = sortOrder == "Date" ? "date_desc" : "Date";
        //    var employees = from s in _context.ListEmployee
        //                   select s;
        //    switch (sortOrder)
        //    {
        //        case "name_desc":
        //            employees = employees.OrderByDescending(s => s.Nama);
        //            break;
        //        case "Date":
        //            employees = employees.OrderBy(s => s.TanggalLahir);
        //            break;
        //        case "date_desc":
        //            employees = employees.OrderByDescending(s => s.TanggalLahir);
        //            break;
        //        case "gender":
        //            employees = employees.OrderByDescending(s => s.JenisKelamin);
        //            break;
        //        case "gender_desc": 
        //            employees = employees.OrderBy(s => s.JenisKelamin);
        //            break;
        //        case "departemen":
        //            employees = employees.OrderByDescending(s => s.Departemen);
        //            break;
        //        case "departemen_desc":
        //            employees = employees.OrderBy(s => s.Departemen);
        //            break;
        //        case "jabatan":
        //            employees = employees.OrderByDescending(s => s.Jabatan);
        //            break;
        //        case "jabatan_desc":
        //            employees = employees.OrderBy(s => s.Jabatan);
        //            break;
        //        default:
        //            employees = employees.OrderBy(s => s.Nama);
        //            break;
        //    }
        //    return View(employees.ToList());
        //}

        // GET: ListEmployees/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: ListEmployees/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Nik,Nama,Alamat,TanggalLahir,JenisKelamin,Departemen,Jabatan")] ListEmployee listEmployee)
        {
            if (ModelState.IsValid)
            {
                //if(listEmployee.Id == 0)
                //{
                //    listEmployee.Id = 0;
                //}
                //listEmployee.Id += 1;
                _context.Add(listEmployee);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(listEmployee);
        }

        // GET: ListEmployees/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var listEmployee = await _context.ListEmployee.FindAsync(id);
            if (listEmployee == null)
            {
                return NotFound();
            }
            return View(listEmployee);
        }

        // POST: ListEmployees/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Nik,Nama,Alamat,TanggalLahir,JenisKelamin,Departemen,Jabatan")] ListEmployee listEmployee)
        {
            if (id != listEmployee.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(listEmployee);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ListEmployeeExists(listEmployee.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(listEmployee);
        }

        // GET: ListEmployees/Delete/5

        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var listEmployee = await _context.ListEmployee
                .FirstOrDefaultAsync(m => m.Id == id);
            if (listEmployee == null)
            {
                return NotFound();
            }

            return View(listEmployee);
        }

        // POST: ListEmployees/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var listEmployee = await _context.ListEmployee.FindAsync(id);
            _context.ListEmployee.Remove(listEmployee);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool ListEmployeeExists(int id)
        {
            return _context.ListEmployee.Any(e => e.Id == id);
        }
    }
}
