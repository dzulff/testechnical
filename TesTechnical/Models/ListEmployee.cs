﻿using System;
using System.Collections.Generic;

namespace TesTechnical.Models
{
    public partial class ListEmployee
    {
        public int Id { get; set; }
        public string Nik { get; set; }
        public string Nama { get; set; }
        public string Alamat { get; set; }
        public DateTime TanggalLahir { get; set; }
        public string JenisKelamin { get; set; }
        public string Departemen { get; set; }
        public string Jabatan { get; set; }
    }
}
