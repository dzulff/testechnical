﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace TesTechnical.Models
{
    public partial class TesTechnicalContext : DbContext
    {
        public TesTechnicalContext()
        {
        }

        public TesTechnicalContext(DbContextOptions<TesTechnicalContext> options)
            : base(options)
        {
        }

        public virtual DbSet<ListEmployee> ListEmployee { get; set; }

//        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
//        {
//            if (!optionsBuilder.IsConfigured)
//            {
//#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
//                optionsBuilder.UseNpgsql("Host=localhost;Database=TesTechnical;User id=postgres;Password=1234567");
//            }
//        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("ProductVersion", "2.2.6-servicing-10079");

            modelBuilder.Entity<ListEmployee>(entity =>
            {
                
                entity.Property(e => e.Id)
                    .HasColumnName("ID")
                    .HasColumnType("integer");

                entity.Property(e => e.Alamat)
                    .IsRequired()
                    .HasColumnName("Alamat ")
                    .HasColumnType("character varying");

                entity.Property(e => e.Departemen)
                    .IsRequired()
                    .HasColumnType("character varying");

                entity.Property(e => e.Jabatan)
                    .IsRequired()
                    .HasColumnType("character varying");

                entity.Property(e => e.Nik)
                    .IsRequired()
                    .HasColumnName("NIK")
                    .HasColumnType("character varying");

                entity.Property(e => e.Nama)
                    .IsRequired()
                    .HasColumnName("Nama")
                    .HasColumnType("character varying");

                entity.Property(e => e.TanggalLahir).HasColumnType("date");
            });
        }
    }
}
